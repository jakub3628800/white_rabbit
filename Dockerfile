FROM phpunit/phpunit:5.5.0

COPY . /white_rabbit

ENTRYPOINT phpunit /white_rabbit/test