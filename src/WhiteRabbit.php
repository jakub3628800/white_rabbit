<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $filecontents = file_get_contents($filePath);       //read file to a string
        $filecontents = strtolower($filecontents);          // lowercase the string
        $filecontents = preg_replace('/[^a-z]/i','', $filecontents); // remove special characters
        $charinfo = count_chars ($filecontents,1);                   // get info about character occurences - we get an array of char => occurence
                                                            // only with a-z , no UPPERCASE
        return $charinfo;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        asort($parsedFile);                   //sort the array
        $size = count($parsedFile);           // number of letters used in the text    
        $medianIndex = floor(($size+1)/2)-1;  // find the median index (-1 at the end because of 0 based index of array)
        
        // now find the $medianIndex entry in our Associative array

        $keys = array_keys($parsedFile);
        $key = $keys[$medianIndex];         // our median letter
        $value = $parsedFile[$key];         // our occurences of the letter
        
        $occurrences = $value;
        return chr($key);
    }
}