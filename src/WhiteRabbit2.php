<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        
        //define the coins
        $result = array(
                '100' => 0,
                '50'   => 0,
                '20'   => 0,
                '10'  => 0,
                '5'  => 0,
                '2'  => 0,
                '1' => 0
            );
        // descending order of the keys is important, either define the coins like this, or sort them before entering the loop

       foreach ($result as $i => $val){

            $numberOfCoins = floor($amount/$i);    // integer division - how many times does our coin fit into the amount ?
            $amount = $amount - $numberOfCoins*$i;
            $result[$i]=$numberOfCoins;
        
       }
    
    return $result;

    }
}