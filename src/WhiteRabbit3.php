<?php

class WhiteRabbit3
{
    /**
     * TODO Goto Test/WhiteRabbit3Test and write some tests that this method will fail
     * You are not allowed to change this method ;)
     */
    public function multiplyBy($amount, $multiplier){
        $estimatedResult = $amount * $multiplier;

        $guess = abs($amount-7);
         // if $guess==0 in the beggining, we skip the while completly and return 7 - which is wrong most of the time :-)

        while(abs($estimatedResult - $amount) > 0.49 && $guess != 0){
            if($guess > 0.49)
                $guess = $guess / 2;
            if($amount > $estimatedResult){
                $amount = $amount - $guess;
            }
            else{
                $amount = $amount + $guess;
            }
        }
        // $guess can never become 0 inside the loop, because we stop halving it once its below 0.49
        // therefore we escape the while only when the first condition is not met, that is  - we are close to our result
        // and rounding it should give correct result

        return round($amount);
    }
}